package rest_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/go-kit/kit/log"

	inmem "gitlab.com/phlo/so1-bid-tracker/pkg/in-mem"
	. "gitlab.com/phlo/so1-bid-tracker/pkg/rest"
)

// just a draft, but it works
func TestRestIntegration(t *testing.T) {
	bt := inmem.NewTracker()
	ts := httptest.NewServer(MakeHTTPHandler(bt, log.NewNopLogger()))
	defer ts.Close()

	// todo use the go-kit client
	res, err := http.Post(fmt.Sprintf("%s/add-bid", ts.URL), "application/json",
		strings.NewReader("{\"item\":1, \"user\": 2, \"bid\": 134}"))
	if err != nil {
		t.Errorf("failed while sending the request: %v", err)
	}
	content, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		t.Errorf("failed while reading response body: %v", err)
	}

	if strings.TrimSpace(string(content)) != "{}" {
		t.Errorf("expected {}, got %s", content)
	}
}
