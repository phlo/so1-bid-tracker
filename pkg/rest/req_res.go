package rest

import "gitlab.com/phlo/so1-bid-tracker/pkg/model"

type AddBidRequest struct {
	Item int
	User int
	Bid  int
}

type errorer interface {
	error() error
}

type AddBidResponse struct {
	Err error `json:"err,omitempty"`
}

func (r AddBidResponse) error() error {
	return r.Err
}

type GetCurrentWinningBidRequest struct {
	Item int
}

type GetCurrentWinningBidResponse struct {
	UserBid *model.UserBid
	Err     error `json:"err,omitempty"`
}

func (r GetCurrentWinningBidResponse) error() error {
	return r.Err
}

type GetBidsRequest struct {
	Item int
}

type GetBidsResponse struct {
	Bids []*model.UserBid
	Err  error `json:"err,omitempty"`
}

func (r GetBidsResponse) error() error {
	return r.Err
}

type GetItemsRequest struct {
	User int
}

type GetItemsResponse struct {
	Items []*model.Item
	Err   error `json:"err,omitempty"`
}

func (r GetItemsResponse) error() error {
	return r.Err
}
