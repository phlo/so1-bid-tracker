package rest

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/transport"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"gitlab.com/phlo/so1-bid-tracker/pkg/model"
)

var (
	ErrMissingHttpParam     = errors.New("param missing from request")
	ErrInvalidParam         = errors.New("param missing from request")
	ErrInvalidNaturalNumber = errors.New("param is not a natural number")
)

func getFromRequest(name string, r *http.Request) (int, error) {
	vars := mux.Vars(r)

	val, ok := vars[name]
	if !ok {
		return 0, ErrMissingHttpParam
	}

	i, err := strconv.Atoi(val)
	if err != nil {
		return 0, ErrInvalidParam
	}

	if i < 0 {
		return 0, ErrInvalidNaturalNumber
	}

	return i, nil
}

func decodeAddBidRequest(_ context.Context, r *http.Request) (interface{}, error) {
	req := AddBidRequest{}

	if e := json.NewDecoder(r.Body).Decode(&req); e != nil {
		return nil, e
	}

	return req, nil
}

func decodeGetCurrentWinningBidRequest(_ context.Context, r *http.Request) (interface{}, error) {
	item, err := getFromRequest("item", r)
	if err != nil {
		return nil, err
	}

	return GetCurrentWinningBidRequest{Item: item}, nil
}

func decodeGetBidsRequest(_ context.Context, r *http.Request) (interface{}, error) {
	item, err := getFromRequest("item", r)
	if err != nil {
		return nil, err
	}

	return GetBidsRequest{Item: item}, nil
}

func decodeGetItemsRequest(_ context.Context, r *http.Request) (interface{}, error) {
	user, err := getFromRequest("user", r)
	if err != nil {
		return nil, err
	}

	return GetItemsRequest{User: user}, nil
}

func MakeHTTPHandler(bt model.BidTracker, logger log.Logger) http.Handler {
	r := mux.NewRouter()
	e := MakeServerEndpoints(bt)
	options := []httptransport.ServerOption{
		httptransport.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
		httptransport.ServerErrorEncoder(encodeError),
	}

	r.Methods(http.MethodPost).Path("/add-bid").Handler(httptransport.NewServer(
		e.AddBid, decodeAddBidRequest, encodeResponse, options...))
	r.Methods(http.MethodGet).Path("/current-winning-bid/{item}").Handler(httptransport.NewServer(
		e.GetCurrentWinningBid, decodeGetCurrentWinningBidRequest, encodeResponse, options...))
	r.Methods(http.MethodGet).Path("/bids/{item}").Handler(httptransport.NewServer(
		e.GetBids, decodeGetBidsRequest, encodeResponse, options...))
	r.Methods(http.MethodGet).Path("/items/{user}").Handler(httptransport.NewServer(
		e.GetItems, decodeGetItemsRequest, encodeResponse, options...))

	return r
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		// Not a Go kit transport error, but a business-logic error.
		// Provide those as HTTP errors.
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	return json.NewEncoder(w).Encode(response)
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(codeFrom(err))
	_ = json.NewEncoder(w).Encode(map[string]interface{}{"error": err.Error()})
}

func codeFrom(err error) int {
	switch err {
	case ErrMissingHttpParam:
		return http.StatusNotFound
	case ErrInvalidNaturalNumber, ErrInvalidParam:
		return http.StatusBadRequest
	default:
		return http.StatusInternalServerError
	}
}
