package rest

import (
	"time"

	"github.com/go-kit/kit/log"
	"gitlab.com/phlo/so1-bid-tracker/pkg/model"
)

type Middleware func(tracker model.BidTracker) model.BidTracker

func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next model.BidTracker) model.BidTracker {
		return &loggingMiddleware{
			next:   next,
			logger: logger,
		}
	}
}

type loggingMiddleware struct {
	next   model.BidTracker
	logger log.Logger
}

func (mw *loggingMiddleware) AddBid(item model.Item, user model.User, bid model.Bid) (err error) {
	defer func(begin time.Time) {
		_ = mw.logger.Log("method", "AddBid", "took", time.Since(begin), "err", err,
			"item", item,
			"user", user,
			"bid", bid)
	}(time.Now())

	return mw.next.AddBid(item, user, bid)
}

func (mw *loggingMiddleware) GetCurrentWinningBid(item model.Item) (ub *model.UserBid, err error) {
	defer func(begin time.Time) {
		_ = mw.logger.Log("method", "GetCurrentWinningBid", "took", time.Since(begin), "err", err,
			"item", item)
	}(time.Now())

	return mw.next.GetCurrentWinningBid(item)
}

func (mw *loggingMiddleware) GetBids(item model.Item) (bids []*model.UserBid, err error) {
	defer func(begin time.Time) {
		_ = mw.logger.Log("method", "GetBids()", "took", time.Since(begin), "err", err,
			"item", item)
	}(time.Now())

	return mw.next.GetBids(item)
}

func (mw *loggingMiddleware) GetItems(user model.User) (items []*model.Item, err error) {
	defer func(begin time.Time) {
		_ = mw.logger.Log("method", "GetItems", "took", time.Since(begin), "err", err,
			"user", user)
	}(time.Now())

	return mw.next.GetItems(user)
}
