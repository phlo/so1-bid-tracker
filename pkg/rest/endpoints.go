package rest

import (
	"context"

	"github.com/go-kit/kit/endpoint"
	"gitlab.com/phlo/so1-bid-tracker/pkg/model"
)

type Endpoints struct {
	AddBid               endpoint.Endpoint
	GetCurrentWinningBid endpoint.Endpoint
	GetBids              endpoint.Endpoint
	GetItems             endpoint.Endpoint
}

func MakeServerEndpoints(bt model.BidTracker) Endpoints {
	return Endpoints{
		AddBid:               MakeAddBidEndpoint(bt),
		GetCurrentWinningBid: MakeGetCurrentWinningBidEndpoint(bt),
		GetBids:              MakeGetBidsEndpoint(bt),
		GetItems:             MakeGetItemsEndpoint(bt),
	}
}

func MakeAddBidEndpoint(bt model.BidTracker) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (response interface{}, err error) {
		req := request.(AddBidRequest)

		err = bt.AddBid(
			model.Item(req.Item),
			model.User(req.User),
			model.Bid(req.Bid),
		)

		return AddBidResponse{Err: err}, nil
	}
}

func MakeGetCurrentWinningBidEndpoint(bt model.BidTracker) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (response interface{}, err error) {
		req := request.(GetCurrentWinningBidRequest)
		var userBid *model.UserBid

		userBid, err = bt.GetCurrentWinningBid(model.Item(req.Item))

		return GetCurrentWinningBidResponse{UserBid: userBid, Err: err}, nil
	}
}

func MakeGetBidsEndpoint(bt model.BidTracker) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (response interface{}, err error) {
		req := request.(GetBidsRequest)

		var userBids []*model.UserBid

		userBids, err = bt.GetBids(model.Item(req.Item))

		return GetBidsResponse{Bids: userBids, Err: err}, nil
	}
}

func MakeGetItemsEndpoint(bt model.BidTracker) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (response interface{}, err error) {
		req := request.(GetItemsRequest)

		var items []*model.Item

		items, err = bt.GetItems(model.User(req.User))

		return GetItemsResponse{Items: items, Err: err}, nil
	}
}
