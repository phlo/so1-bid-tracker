package in_mem_test

import (
	"fmt"
	"math/rand"
	"testing"

	. "gitlab.com/phlo/so1-bid-tracker/pkg/in-mem"
	"gitlab.com/phlo/so1-bid-tracker/pkg/model"
)

func TestTracker_AddBid(t1 *testing.T) {
	tracker := NewTracker()
	testAddBid(t1, tracker, addBidTests{
		{
			name:    "add bid",
			args:    addBidArgs{1, 7, 4},
			wantErr: false,
		},
		{
			name:    "increase bid",
			args:    addBidArgs{1, 7, 5},
			wantErr: false,
		},
		{
			name:    "add smaller bid",
			args:    addBidArgs{user: 1, item: 7, bid: 3},
			wantErr: true,
		},
		{
			name:    "add bid for different user",
			args:    addBidArgs{2, 7, 3},
			wantErr: false,
		},
		{
			name:    "add bid for different item",
			args:    addBidArgs{1, 6, 2},
			wantErr: false,
		},
		{
			name:    "add bid 0",
			args:    addBidArgs{1, 6, 0},
			wantErr: true,
		},
		{
			name:    "add negative bid",
			args:    addBidArgs{1, 6, -1},
			wantErr: true,
		},
	})
}

func TestTracker_GetCurrentWinningBid(t1 *testing.T) {
	tracker := NewTracker()
	_ = tracker.AddBid(1, 2, 3)
	_ = tracker.AddBid(1, 1, 2)

	testGetCurrentWinningBid(t1, tracker, getCurrentWinnerBidTests{
		{
			name:      "current winning bid",
			args:      getCurrentWinnerBidArgs{item: 1},
			shouldGet: &model.UserBid{User: 2, Bid: 3},
			wantErr:   false,
		},
		{
			name:      "no bids",
			args:      getCurrentWinnerBidArgs{item: 2},
			shouldGet: nil,
			wantErr:   false,
		},
	})
}

func TestTracker_GetBids(t1 *testing.T) {
	tracker := NewTracker()
	_ = tracker.AddBid(1, 2, 3)
	_ = tracker.AddBid(3, 2, 4)
	_ = tracker.AddBid(3, 2, 1)

	testGetBids(t1, tracker, getBidsTests{
		{
			name:      "bids for item",
			args:      getBidsArgs{item: 3},
			shouldGet: []*model.UserBid{{User: 2, Bid: 4}},
			wantErr:   false,
		},
		{
			name:    "no bids",
			args:    getBidsArgs{item: 2},
			wantErr: true,
		},
	})
}

func TestTracker_GetItems(t1 *testing.T) {
	tracker := NewTracker()
	_ = tracker.AddBid(1, 1, 3)
	_ = tracker.AddBid(2, 1, 5)
	_ = tracker.AddBid(1, 2, 2)

	item1 := model.Item(1)
	item2 := model.Item(2)
	testGetItems(t1, tracker, getItemsTests{
		{
			name:      "get stuff",
			args:      getItemsArgs{user: 1},
			shouldGet: []*model.Item{&item1, &item2},
			wantErr:   false,
		},
		{
			name:    "error path",
			args:    getItemsArgs{user: 3},
			wantErr: true,
		},
	})
}

func TestTracker_Integration(t1 *testing.T) {
	tracker := NewTracker()

	testAddBid(t1, tracker, addBidTests{
		{
			name:    "add bid",
			args:    addBidArgs{1, 7, 4},
			wantErr: false,
		},
	})
	testGetCurrentWinningBid(t1, tracker, getCurrentWinnerBidTests{
		{
			name:      "get current winning bid",
			args:      getCurrentWinnerBidArgs{item: 7},
			shouldGet: &model.UserBid{User: 1, Bid: 4},
			wantErr:   false,
		},
	})
	testGetBids(t1, tracker, getBidsTests{
		{
			name:      "get bids for item",
			args:      getBidsArgs{item: 7},
			shouldGet: []*model.UserBid{{User: 1, Bid: 4}},
			wantErr:   false,
		},
	})

	item7 := model.Item(7)
	testGetItems(t1, tracker, getItemsTests{
		{
			name:      "get items for user",
			args:      getItemsArgs{user: 1},
			shouldGet: []*model.Item{&item7},
			wantErr:   false,
		},
	})
}

func BenchmarkTracker_AddBid(b *testing.B) {
	tracker := NewTracker()
	for i := 0; i < b.N; i++ {
		_ = tracker.AddBid(1, 2, 2)
	}
}

func BenchmarkTracker_GetCurrentWinningBid(b *testing.B) {
	tracker := NewTracker()

	sizesToBenchmark := []struct {
		name string
		size int
	}{
		// ns/op was growing exponentially before introducing the itemWinningBids cache
		{name: "512", size: 512},
		{name: "1K", size: 1024},
		{name: "64K", size: 64 * 1024},
		{name: "128K", size: 128 * 1024},
		{name: "256K", size: 512 * 1024},
		{name: "512K", size: 512 * 1024},
		{name: "1M", size: 1024 * 1024},
	}

	for i, size := range sizesToBenchmark {
		for j := 0; j < size.size; j++ {
			_ = tracker.AddBid(
				model.Item(i),
				model.User(j),
				model.Bid(rand.Intn(3000)))
		}
	}

	for i, size := range sizesToBenchmark {
		b.Run(fmt.Sprintf("size %s", size.name), func(b *testing.B) {
			for k := 0; k < b.N; k++ {
				_, _ = tracker.GetCurrentWinningBid(model.Item(i))
			}
		})
	}
}
