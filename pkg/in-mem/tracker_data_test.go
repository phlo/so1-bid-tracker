package in_mem_test

import (
	"reflect"
	"testing"

	"github.com/davecgh/go-spew/spew"
	inmem "gitlab.com/phlo/so1-bid-tracker/pkg/in-mem"
	"gitlab.com/phlo/so1-bid-tracker/pkg/model"
)

type addBidArgs struct {
	user model.User
	item model.Item
	bid  model.Bid
}

type addBidTests []struct {
	name    string
	args    addBidArgs
	wantErr bool
}

type getCurrentWinnerBidArgs struct {
	item model.Item
}

type getCurrentWinnerBidTests []struct {
	name      string
	args      getCurrentWinnerBidArgs
	shouldGet *model.UserBid
	wantErr   bool
}

type getBidsArgs struct {
	item model.Item
}

type getBidsTests []struct {
	name      string
	args      getBidsArgs
	shouldGet []*model.UserBid
	wantErr   bool
}

type getItemsArgs struct {
	user model.User
}

type getItemsTests []struct {
	name      string
	args      getItemsArgs
	shouldGet []*model.Item
	wantErr   bool
}

func testAddBid(t1 *testing.T, tracker *inmem.Tracker, tests addBidTests) {
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			if err := tracker.AddBid(tt.args.item, tt.args.user, tt.args.bid); (err != nil) != tt.wantErr {
				t1.Errorf("AddBid() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func testGetCurrentWinningBid(t1 *testing.T, tracker *inmem.Tracker, tests getCurrentWinnerBidTests) {
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			gotWinningBid, err := tracker.GetCurrentWinningBid(tt.args.item)
			if (err != nil) != tt.wantErr {
				t1.Errorf("GetCurrentWinningBid() error = %v, wantErr %v", err, tt.wantErr)
			}
			if gotWinningBid != nil {
				if *gotWinningBid != *tt.shouldGet {
					t1.Errorf("GetCurrentWinningBid() gotWinningBid = %+v, want %+v", gotWinningBid, tt.shouldGet)
				}
			} else {
				if tt.shouldGet != nil {
					t1.Errorf("GetCurrentWinningBid() gotWinningBid = %+v, want nil", gotWinningBid)
				}
			}
		})
	}
}

func testGetBids(t1 *testing.T, tracker *inmem.Tracker, tests getBidsTests) {
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			gotBids, err := tracker.GetBids(tt.args.item)
			if (err != nil) != tt.wantErr {
				t1.Errorf("GetBids() error = %v, wantErr %v", err, tt.wantErr)
			}
			if len(gotBids) != len(tt.shouldGet) {
				t1.Error(spew.Sprintf("expected %v, got %v", gotBids, tt.shouldGet))
			}
			for i, gotUserBid := range gotBids {
				if !reflect.DeepEqual(*gotUserBid, *tt.shouldGet[i]) {
					t1.Errorf("expected %v, got %v", *gotUserBid, *tt.shouldGet[i])
				}
			}
		})
	}
}

func testGetItems(t1 *testing.T, tracker *inmem.Tracker, tests getItemsTests) {
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			gotItems, err := tracker.GetItems(tt.args.user)
			if (err != nil) != tt.wantErr {
				t1.Errorf("GetItems() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !reflect.DeepEqual(gotItems, tt.shouldGet) {
				t1.Errorf("GetItems() gotItems = %v, want %v", spew.Sprint(gotItems), spew.Sprint(tt.shouldGet))
			}
		})
	}
}
