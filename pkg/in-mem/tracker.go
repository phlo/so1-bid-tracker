package in_mem

import (
	"errors"
	"fmt"
	"sync"

	"gitlab.com/phlo/so1-bid-tracker/pkg/model"
)

type userItem struct {
	user model.User
	item model.Item
}

type Tracker struct {
	bids            map[userItem]model.Bid
	itemUsers       map[model.Item][]*model.User
	userItems       map[model.User][]*model.Item
	itemWinningBids map[model.Item]*model.UserBid
	*sync.RWMutex
}

func NewTracker() *Tracker {
	return &Tracker{
		bids:            make(map[userItem]model.Bid),
		itemUsers:       make(map[model.Item][]*model.User),
		userItems:       make(map[model.User][]*model.Item),
		itemWinningBids: make(map[model.Item]*model.UserBid),
		RWMutex:         &sync.RWMutex{},
	}
}

func (t Tracker) AddBid(item model.Item, user model.User, bid model.Bid) error {
	t.Lock()
	defer t.Unlock()

	if bid < 1 {
		return errors.New("bid is supposed to be > 0")
	}

	ui := userItem{user, item}

	if existingBid, alreadyExists := t.bids[ui]; alreadyExists && existingBid >= bid {
		return fmt.Errorf("user %v is supposed to bid more for item %v", user, item)
	}

	if existingWinningBid, ok := t.itemWinningBids[item]; !ok {
		t.itemWinningBids[item] = &model.UserBid{User: user, Bid: bid}
	} else if existingWinningBid.Bid < bid {
		existingWinningBid.Bid = bid
		t.itemWinningBids[item] = existingWinningBid
	}

	t.bids[ui] = bid
	// todo don't add twice
	t.itemUsers[ui.item] = append(t.itemUsers[ui.item], &ui.user)
	// todo don't add twice
	t.userItems[ui.user] = append(t.userItems[ui.user], &ui.item)

	return nil
}

func (t Tracker) GetCurrentWinningBid(item model.Item) (bid *model.UserBid, err error) {
	t.RLock()
	defer t.RUnlock()

	if wb, ok := t.itemWinningBids[item]; ok {
		return wb, nil
	}

	return
}

func (t Tracker) GetBids(item model.Item) (bids []*model.UserBid, err error) {
	t.RLock()
	defer t.RUnlock()

	var (
		users    []*model.User
		hasUsers bool
	)

	if users, hasUsers = t.itemUsers[item]; !hasUsers {
		return nil, errors.New("nothing was bidden yet")
	}

	for _, user := range users {
		if bid, exists := t.bids[userItem{*user, item}]; exists {
			bids = append(bids, &model.UserBid{User: *user, Bid: bid})
		}
	}

	return
}

func (t Tracker) GetItems(user model.User) (items []*model.Item, err error) {
	t.RLock()
	defer t.RUnlock()

	if _, hasItems := t.userItems[user]; !hasItems {
		return nil, fmt.Errorf("no items were bidden on yet by %v", user)
	}

	return t.userItems[user], nil
}
