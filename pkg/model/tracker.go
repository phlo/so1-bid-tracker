package model

type User int

type Item int

type Bid int

type UserBid struct {
	User User
	Bid  Bid
}

type BidTracker interface {
	AddBid(item Item, user User, bid Bid) error
	GetCurrentWinningBid(item Item) (*UserBid, error)
	GetBids(item Item) (bids []*UserBid, err error)
	GetItems(user User) (items []*Item, err error)
}
