module gitlab.com/phlo/so1-bid-tracker

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/go-kit/kit v0.9.0
	github.com/go-logfmt/logfmt v0.5.0 // indirect
	github.com/gorilla/mux v1.7.3
	github.com/sirupsen/logrus v1.4.2 // indirect
)
