# Auction Bid Tracker

You have been asked with building part of a simple online auction system which will allow users to concurrently bid on items for sale. The system needs to be built in Go and/or Python.

Please, provide a bid-tracker interface and concrete implementation with the following functionality:
* record a user’s bid on an item;
* get the current winning bid for an item;
* get all the bids for an item;
* get all the items on which a user has bid;
* build simple REST API to manage bids.

You are not required to implement a GUI (or CLI) or persistent store (events are for reporting only). 
You may use any appropriate libraries to help.
Test the performance of your solution.
Please include the full source code, program parameters & instructions with your solution. 
Describe chosen data structures & concurrency approach.

## solution mentions

The interface is in `pkg/model/tracker.go`. 

Its in-memory implementation is in `pkg/in-mem` and it comes with tests and benchmarks.
The implementation is designed to eat ram rather than cpu. It uses redundancy for storing relations between entities
and for caching (in order to avoid the pricy max_bid calculation).

For concurrency I chose to use locks because it was easier, but in the end this approach didn't allow me 
to test concurrency, as would have been the case should I have chosen to use channels. 
It should still pass the `-race` detector though.

The REST interface is in `pkg/rest` (it has a test). `go run cmd/main.go -addr localhost:14051`

A notable lib I used is [go-kit](https://gokit.io/) for the rest handlers, because I just wanted to play with
such a useful tool for microservices. The code is now harder to read and maintain, but I HAD TO go with go-kit :D

### tests
```
go test -race ./...
```

### benchmarks
```
go test -run=XXX -bench=. ./...
```