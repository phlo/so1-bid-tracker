package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-kit/kit/log"
	inmem "gitlab.com/phlo/so1-bid-tracker/pkg/in-mem"
	"gitlab.com/phlo/so1-bid-tracker/pkg/model"
	"gitlab.com/phlo/so1-bid-tracker/pkg/rest"
)

func main() {
	var (
		httpAddr = flag.String("addr", ":8080", "HTTP listen address")
	)
	flag.Parse()

	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.With(logger, "ts", log.DefaultTimestampUTC)
		logger = log.With(logger, "caller", log.DefaultCaller)
	}

	var bt model.BidTracker = inmem.NewTracker()
	bt = rest.LoggingMiddleware(logger)(bt)

	h := rest.MakeHTTPHandler(bt, log.With(logger))

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	go func() {
		_ = logger.Log("transport", "HTTP", "addr", *httpAddr)
		errs <- http.ListenAndServe(*httpAddr, h)
	}()

	_ = logger.Log("exit", <-errs)
}
